## ALEX123 converter
The ALEX123 converter extracts averaged profile and centroid mode data from .RAW files.  
The output is written to a directory tree matching the scan filters in the processed .RAW files.  
 
**Installation**  
Install Python 3.6.1 (64-bit) (Make sure that Python is installed at C:\Python36) (https://www.python.org/ftp/python/3.6.1/python-3.6.1-amd64.exe).  
Install MSFileReader (64-bit) (”MSFileReader_x64_3.1 SP4.exe”) (Thermo Fisher Scientific) to use the Python-based ALEX123 converter (https://thermo.flexnetoperations.com/control/thmo/login?).  
Install comtypes (http://mslipidomics.info/contents/?page_id=467).  

**Using the program:**  
Download and unpack the file: "ALEX converter.zip"
Follow the instructions specified in readme.docx.  

**Advanced operation of the program:**  
The ALEX converter is guided by the text file: config_file.txt  
This files contains the following operational fields:  

{  
“input_dir” : “raw”,  
“output_dir” : “txt”,  
“state_file” : “state.txt”,  
“include_filters” : “*”,  
“data_format” : “profile”,  
“msfr_method” : “GetAverageMassList”,  
“named_averages” : {  
“pos” : {  
“acE8_pos_direct_151_24.RAW” : [  
[“myavg”, 13,15,17],  
[“otheravg”, [30,70] ]  
] } },  
“skip_scans” : {  
“(default)” : [1,1],  
“pos” : {  
“acE8_pos_direct_151_23.RAW” : [18,2],  
“acE8_pos_direct_151_16.RAW” : [14,2],  
“acE8_pos_direct_151_28.RAW” : [13,2],  
“acE8_pos_direct_151_05.RAW” : [2,35]  
},  
“neg” : {  
“acE8_neg_direct_21_01.RAW” : [27,2],  
“acE8_neg_direct_21_23.RAW” : [2,14]  
} }}  
  
– input_dir and output_dir are the directory trees containing the input RAW files and the output txt files, respectively. All output directories are automatically created, if they do not exist.  
  
– state_file is a file recording the settings (skipped scans) that each file has been processed with. If it exists, it is read initially, and files already processed are skipped, if the settings have not changed. The file is rewritten after processing each RAW files, so you can terminate the processing (with e.g. ctrl+C) and resume later. Delete the file, if you want to reprocess all .RAW files.  
  
– include_filters this option allows of export data from specific ‘scan filters’.  
For example, using “FTMS*” will only export FTMS data, and not ITMS data.  
For example2, using “*ms2*” will only export MS2 data, and not MS1 and MS3 data.  
  
– data_format this option allows you to export either “profile” or “centroid” mode data.  
  
– msfr_method this option allows you to export data using either the MSFileReader functions:  
“GetAverageMassList” (should be used for exporting data in profile mode), or  
“GetAveragedLabelData” (should be used for exporting data in centroid mode).  
  
– named_averages this option enables you to export data from specific scan numbers or intervals.  
For example, [“myavg”, 13,15,17] will average scans 13, 15 and 17, and write an output file with a suffix _myavg.  
For example2, [“otheravg”, [30,70] ] will average all scans between 30 and 70, and write an output file with a suffix _otheravg.  
  
– skip_scans specifies how many scans to skip at the beginning and end of the RAW files. The setting is specified as a nested structure representing the input directory tree. Any level allows a “(default)” item overriding any higher level default values. A single global setting can be specified by:  
  
“skip_scans” : { “(default)” : [1,1] }  
  
– Note that the file consists of “key : value” pairs separated by commas. For skip_scans, a nested structure enclosed in curly braces “{}” is a value in this regard.  
  
– Run the ALEX converter by either:  
– drag and drop the configuration file (config_file.txt) onto the icon of “ALEX_converter_2017_170707.py” (or a shortcut to it), or  
– use the .cmd file (convert_RAW_to_TXT.cmd), or  
– run the extraction from the command line: …\ALEX_converter_2017_170707.py “config_file.txt”  
