## Download individual ALEX123 software modules

*Note* that all ALEX123 software modules can be downloaded as part the Example dataset 1.

**► ALEX123 user guide**  
User guide that outlines how the different ALEX123 modules work.  

**► ALEX123 converter**  
This tool extracts and averages spectral data stored in proprietary .RAW files.  

**► ALEX123 target list generator**  
This software tool is used for making “target lists” for searching FTMS, MS2 and MS3 data.  

**► ALEX123 isotope compiler**  
This tool adds 13C isotope information to “target lists” (.txt files).  

**► ALEX123 extractor**  
This software tool uses information in “target lists” to search for lipid species and fragment m/z values.  

**► ALEX123 unifier**  
This software tool concatenates results of multiple searches into a single output file.  

**► ALEX123 script for 13C deisotoping**  
This SAS Enterprise Guide-based script performs deisotoping of FTMS data.  
*It is recommended to download this script as part of the example dataset.*

**► ALEX123 script for high confidence lipid identification**  
This SAS Enterprise Guide-based script performs lipid identification using MALDI-based FTMS and ITMS2 data.  
*It is recommended to download this script as part of the example dataset.*
 

