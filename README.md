# ALEX123

ALEX123 (Analysis of Lipid EXperiments, for processing FTMS, MS2 and MS3 data) is a software framework for analyzing high resolution shotgun and MALDI-based lipidomics data.

+ The procedure for high confidence lipid identification using MALDI-based FTMS and ITMS2 data, described in **[Ellis et al. Nat Methods (2018)](https://doi.org/10.1038/s41592-018-0010-6)**, can be downloaded from the library "**Example dataset 1**".  
+ **Note** that *all* ALEX123 software modules are downloaded as part of the Example dataset.
+ The Example dataset also includes a user guide (.pdf).
  
+ Individual ALEX123 software modules can be downloaded separately from the library: "ALEX123 software modules".
  

**NOTICE** that there are no warranties or representations as to the fitness of this equipment/software for any particular purpose and no responsibility or contingent liability, including indirect or consequential damages, for any use to which the user may put the equipment/software provided, or for any adverse circumstances arising therefrom.


## Requirements

+ The ALEX123 software framework is operational on desktop and labtop computers running Microsoft Windows 7 or 10 (Professional 64-bit), and having at least a 2.7 GHz dual core processor with at least 8 GB RAM. The user should have administrator rights.
+ Install Java (64-bit) to use Java-based ALEX123 software modules (https://java.com/en/download/manual.jsp).
+ Install MSFileReader (64-bit) (”MSFileReader_x64_3.1 SP4.exe”) (Thermo Fisher Scientific) to use the Python-based ALEX123 converter (https://thermo.flexnetoperations.com/control/thmo/login?).
+ Install Python 3.6.1 (64-bit) and comtypes to use the Python-based ALEX123 converter.
+ Install SAS 9.2 with SAS Enterprise Guide 5.1 (or later versions) to use scripts for automated high confidence lipid identification (file extension .epg) (https://www.sas.com/en_us/software/enterprise-guide.html).
+ Install Tableau Desktop (version 10.2 or later) to use and modify data visualization templates (file extensions .twb) (http://www.tableausoftware.com/products/desktop).

## References

+ Ellis SR, Paine MRL, Eijkel GB, Pauling JK, Husen P, Jervelund M, Hermansson M, Ejsing CS, Heeren RMA. Automated, Parallel Mass Spectrometry Imaging and Structural Identification of Lipids. [Nat Methods. 2018](https://doi.org/10.1038/s41592-018-0010-6).
+ Pauling JK, Hermansson M, Hartler J, Christiansen K, Gallego SF, Peng B, Ahrends R, Ejsing CS. Proposal for a common nomenclature for fragment ions in mass spectra of lipids. [PLoS One. 2017 12(11):e0188394](https://doi.org/10.1371/journal.pone.0188394). 
+ Husen P, Tarasov K, Katafiasz M, Sokol E, Vogt J, Baumgart J, Nitsch R, Ekroos K, Ejsing CS. Analysis of lipid experiments (ALEX): a software framework for analysis of high-resolution shotgun lipidomics data. [PLoS One. 2013 8(11):e79736](https://doi.org/10.1371/journal.pone.0079736).

