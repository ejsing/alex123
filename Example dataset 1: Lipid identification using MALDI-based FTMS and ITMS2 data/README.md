# Example dataset 1: High confidence lipid identification using MALDI-based FTMS and ITMS2 data

+ The Example dataset 1 relates to our publication: **[Ellis et al. Nat Methods (2018)](https://doi.org/10.1038/s41592-018-0010-6)**  
+ The Example dataset 1 (BrainImaging.zip, 93 MB) can be downloaded [here](http://mslipidomics.info/contents/wp-content/uploads/2018/ALEX123_download/BrainImaging.zip)  
+ The example dataset includes *all* ALEX123 software modules (executed using Java and Python), scripts for high confidence lipid identification (executed using SAS Enterprise Guide) and data visualization templates (navigated using Tableau Desktop).  
+ The example dataset also includes a user guide (.pdf).  
+ Note that mass spectrometry data files (.RAW) should be downloaded separately from the MetaboLight repository ([MTBLS597](https://www.ebi.ac.uk/metabolights/reviewerb78b2bf1-aeb6-4244-b967-368e18ed806d)).  
 